var Repository = function (params) {
    "use strict";
    /* eslint-env node */

    var fs = require("fs"),
        run = require("child_process").execSync,
        rimraf = require("rimraf");

    var title = params.title || "",
        directory = global.config.tmpFolder + "/" + title.replace(new RegExp(" ", "g"), ""),
        url = params.url || "",
        linesOfCode = params.linesOfCode || 0,
        numberOfCommits = params.numberOfCommits || 0,
        numberOfFiles = params.numberOfFiles || 0,
        contributors = params.contributors || [],
        logs = [];

    function prepare() {
        fs.mkdirSync(directory);
        return this;
    }

    function checkout() {
        run("git clone " + url + " " + directory + " && cd " + directory + " && git_stats generate");
        return this;
    }

    function getLogs() {
        var logsString;
        run(global.config.gitCommands.getLogs + " > gitstats-logs", {
            cwd: directory
        });

        logsString = fs.readFileSync(directory + "/gitstats-logs").toString().slice(0, -1).replace(/'/g, "\"");
        return JSON.parse("[" + logsString + "]");
    }

    function getContributors() {
        var lines, parts, authors = [];
        run(global.config.gitCommands.getAuthors + " > gitstats-authors", {
            cwd: directory
        });
        lines = fs.readFileSync(directory + "/gitstats-authors").toString().split("\n");
        lines.forEach(function (line) {
            if (line === "") {
                return;
            }
            parts = line.trim().split("\t");
            authors.push({
                name: parts[1],
                commits: parseInt(parts[0])
            });

        });
        return authors;
    }

    function getNumberOfFiles() {
        run(global.config.gitCommands.getNumberOfFiles + " > gitstats-files", {
            cwd: directory
        });
        return parseInt(fs.readFileSync(directory + "/gitstats-files").toString().trim());
    }

    function getLinesOfCode() {
        run(global.config.gitCommands.getLOCStats + " > gitstats-loc", {
            cwd: directory
        });
        return parseInt(fs.readFileSync(directory + "/gitstats-loc").toString().trim());
    }

    function createStats() {
        linesOfCode = getLinesOfCode();
        numberOfFiles = getNumberOfFiles();
        contributors = getContributors();
        numberOfCommits = (function (authors) {
            var count = 0;
            authors.forEach(function (author) {
                count += author.commits;
            });
            return count;
        }(contributors));
        logs = getLogs();
    }

    function updateStats() {
        createStats();
        //traverseHTML();
        //extractLogs();
        return this;
    }

    function clear() {
        rimraf.sync(directory);
        return this;
    }

    function get() {
        var result = {};
        result.title = title;
        result.url = url;
        result.linesOfCode = linesOfCode;
        result.numberOfCommits = numberOfCommits;
        result.numberOfFiles = numberOfFiles;
        result.contributors = contributors;
        result.logs = logs;
        return result;
    }

    return {
        prepare: prepare,
        checkout: checkout,
        updateStats: updateStats,
        clear: clear,
        get: get
    };
};

module.exports = Repository;
