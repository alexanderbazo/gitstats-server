(function () {
    "use strict";
    /* eslint-env node */

    var fs = require("fs"),
        Repository = require("./src/Repository.js"),
        rimraf = require("rimraf");

    var results = [];

    function saveToFile(file) {
        var json = JSON.stringify(results);
        console.log("Saving file: " + file);
        fs.writeFileSync(file, json);
    }

    function clear() {
        rimraf.sync(global.config.tmpFolder);
        return this;
    }

    function updateRepository(repository) {
        var repo = new Repository(repository),
            result = repo.prepare().checkout().updateStats().clear().get();
        return result;
    }

    function loadData() {
        var data = require(global.config.dataFile);
        fs.mkdirSync(global.config.tmpFolder);
        for (var i = 0; i < data.length; i++) {
            results.push(updateRepository(data[i]));
        }
        saveToFile(global.config.dataFile);
        return this;
    }

    function loadConfig() {
        global.config = require("./config.json");
        return this;
    }

    return {
        loadConfig: loadConfig,
        loadData: loadData,
        clear: clear
    };

}().loadConfig().clear().loadData().clear());
